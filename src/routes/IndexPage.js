import React from 'react';
import { Icon } from 'antd';
import { connect } from 'dva';
import styles from './IndexPage.less';
import MyAntUpload from './Myupload';
function IndexPage() {
  return (
    <div className={styles.wrap}>

        <h2>Project</h2>
      <div className={styles.blank}></div>
      <div className={styles.items}>
        <div className={styles.itemsList}>
          <span className={styles.spanI}>开发</span>
          <span className={styles.spanI}>生产编译</span>
          <span className={styles.spanI}>
              <MyAntUpload/>
          </span>

          <span className={styles.spanI}>Zip打包</span>
        </div>
      </div>
      <div className={styles.IconItems}>
        <Icon type="plus" />
        <Icon type="minus" />
        <Icon type="folder-add" />
        <Icon type="setting" />
      </div>
    </div>
  );
}

IndexPage.propTypes = {
};

export default connect()(IndexPage);
