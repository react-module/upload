var fs = require('fs');
path =require('path');
// 对文件或目录进行监视，并且在监视到修改时执行处理；
// fs.watch返回一个fs.FSWatcher对象，拥有一个close方法，用于停止watch操作；
// 当fs.watch有文件变化时，会触发fs.FSWatcher对象的change(err, filename)事件，err错误对象，filename发生变化的文件名
// fs.watch(filename, [options], [listener]);

/**
 * filename, 完整路径及文件名或目录名；
 * [listener(event, filename], 监听器事件，有两个参数：event 为rename表示指定的文件或目录中有重命名、删除或移动操作或change表示有修改，filename表示发生变化的文件路径
 */

var fsWatcher = fs.watch('/Users/eh/ty/upload-my' + '/ftp', function (event,filename) {
  console.log(event);

});
//console.log(fsWatcher instanceof FSWatcher);

fsWatcher.on('change', function (event, filename) {

  console.log(filename + ' 发生变化');
});

//30秒后关闭监视
// setTimeout(function () {
//   console.log('关闭')
//   fsWatcher.close(function (err) {
//     if(err) {
//       console.error(err)
//     }
//     console.log('关闭watch')
//   });
// }, 30000);
